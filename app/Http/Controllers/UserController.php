<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
// use Illuminate\Validation\Rules\Password;
use Illuminate\Support\Facades\Hash;

use App\Models\User;

class UserController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    // use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    // protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('guest')->except('logout');
    }

    public function index()
    {
        return view('auth.member.register');
    }

    public function admin_register_user()
    {
        return view('pages.admin.register-user');
    }

    public function store_register_user(Request $request)
    {
        // dd($request->save_more);
        // $data = new SalaryEmployeesModel;

        $validator = Validator::make($request->all(), [
            'name' => ['required','min:3'],
            'email' => ['required','email:rfc,dns'],
            'password' => ['required', 'confirmed'],
        ]);

        if ($validator->fails()) {
            session()->flash('flash_warning', 'Registration failed!');
            return back()->withErrors($validator);
        }
        $hash_password = Hash::make($request->password);
        // input using eloquent (dont forget setup model fillable)
        $create = User::create([
            'name'=>$request->name,
            'email'=>$request->email,
            'telp'=>$request->telp,
            'address'=>$request->address,
            'roles'=>$request->roles,
            'password'=>$hash_password,
        ]);

        if ($create) {
            if(isset($request->save_more)){
                return back()->with('flash_success','Congratulatin, Registration success.');
            }
            if($request->roles == 'member'){
                return redirect('member')->with('flash_success','Congratulatin, Registration success.');
            }else {
                return redirect('operator')->with('flash_success','Congratulatin, Registration success.');    
            }
        }
    }

    public function admin_member()
    {
        $data['users'] = User::withTrashed()
            ->whereIn('roles',['member'])
            ->paginate(5);// show all data with trashed / softdelete and pagination
        return view('pages.admin.member',$data);
    }

    public function admin_operator()
    {
        $data['sub_menu'] = ['operators'];//,'recent','add_new'
        // $data['users'] = User::all();
        $data['users'] = User::withTrashed()
            ->whereIn('roles',['administrator','admin'])
            ->paginate(5);// show all data with trashed / softdelete and pagination
        // dd($data['users']);
        return view('pages.admin.operator',$data);
    }

    public function admin_user_toggle_delete($id)
    {
        $data = User::onlyTrashed()->where('id',$id);
        $res = ['code'=>400,'status'=>'failed'];
        if ($data->get()->count() > 0) {
            if($data->restore())
            $res = ['code'=>200,'status'=>'restore success'];
        }else{
            $data = User::find($id);
            if($data->delete())
            $res = ['code'=>200,'status'=>'delete success'];
        }
        return response()->json($res);
    }

    public function admin_operator_update(Request $request, $id)
    {
        try {
            $data = User::withTrashed()->find($id);
            if (isset($request->password)) {
                $validator = Validator::make($request->all(), [
                    'password' => ['current_password'],
                    'new_password' => ['required_if:password,true','different:password','string'],
                    'retype_password' => ['same:new_password'],
                ]);
        
                if ($validator->fails()) {
                    session()->flash('flash_warning', 'Update data failed!');
                    return back()->withErrors($validator);
                }
                // $request->user()->fill([
                //     'password' => Hash::make($request->new_password)
                // ])->save();
                $data->password = Hash::make($request->new_password);
            }
            $data->name = $request->name;
            $data->email = $request->email;
            $data->telp = $request->telp;
            $data->address = $request->address;
            
            $data->save();

            $rest = [
                'code' => 200,
                'status' => 'SUCCESS',
                'data' => $data,
            ];
            // return response($rest);
            // return $rest;
            return redirect('operator')->with('flash_success','Congratulatin, update data success.');
        } catch (\Exception $ex) {
            return response()->json([
                'code' => 400,
                'status' => 'ERROR',
                'data' => ['error_desc'=>$ex->getMessage()]
            ]);
        }
        // return response()->json($request);
    }

    public function admin_member_update(Request $request, $id)
    {
        try {
            $data = User::withTrashed()->find($id);
            if (isset($request->password)) {
                $validator = Validator::make($request->all(), [
                    'password' => ['current_password'],
                    'new_password' => ['required_if:password,true','different:password','string'],
                    'retype_password' => ['same:new_password'],
                ]);
        
                if ($validator->fails()) {
                    session()->flash('flash_warning', 'Update data failed!');
                    return back()->withErrors($validator);
                }
                $data->password = Hash::make($request->new_password);
            }
            $data->name = $request->name;
            $data->email = $request->email;
            $data->telp = $request->telp;
            $data->address = $request->address;
            
            $data->save();

            $rest = [
                'code' => 200,
                'status' => 'SUCCESS',
                'data' => $data,
            ];
            return redirect('member')->with('flash_success','Congratulatin, update data success.');
        } catch (\Exception $ex) {
            return response()->json([
                'code' => 400,
                'status' => 'ERROR',
                'data' => ['error_desc'=>$ex->getMessage()]
            ]);
        }
    }
}
