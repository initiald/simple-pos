<?php

return [

    'main_navigation'               => 'MAIN NAVIGATION',
    'blog'                          => 'Blog',
    'pages'                         => 'Pages',
    'account_settings'              => 'ACCOUNT SETTINGS',
    'profile'                       => 'Profile',
    'change_password'               => 'Change Password',
    'multilevel'                    => 'Multi Level',
    'level_one'                     => 'Level 1',
    'level_two'                     => 'Level 2',
    'level_three'                   => 'Level 3',
    'labels'                        => 'LABELS',
    'important'                     => 'Important',
    'warning'                       => 'Warning',
    'information'                   => 'Information',
    'example'                       => 'Example',

    'header_products'               => 'Products',
    'products'                      => 'Product List',
    'product_categories'            => 'Product Categories',
    'sales'                         => 'Sales',
    'suppliers'                     => 'Suppliers',
    'management_users'              => 'Management Users',
    'members'                       => 'Members',
    'operators'                     => 'Operators',
    'add_new'                       => 'Add New',
];
