<?php

return [

    'edit_operator'               => 'Edit Operator',
    'list_operator'               => 'List Operator',
    'detail_operator'             => 'Detail Operator',
    'edit_member'               => 'Edit Member',
    'list_member'               => 'List Member',
    'detail_member'             => 'Detail Member',
    'form_add_new'                => 'Form add new user',
];
