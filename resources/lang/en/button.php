<?php

return [

    'edit'               => 'Edit',
    'detail'               => 'Detail',
    'add'               => 'Add',
    'add_new'               => 'Add New',
    'save'               => 'Save',
    'save_more'           => 'Save & Add More',
    'close'               => 'Close',
];
