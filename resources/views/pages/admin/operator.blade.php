@extends('layouts.app')
@section('page_title', __('menu.operators'))
{{-- @section('has_menu')
    @parent
    <li class="breadcrumb-item"><a href="#">{{__('menu.management_users')}}</a></li>
    @foreach ($sub_menu as $item)
        @if (!$loop->last)
            <li class="breadcrumb-item"><a href="#">{{$item}}</a></li>
        @else
            <li class="breadcrumb-item active">{{$item}}</li>        
        @endif
    @endforeach
@endsection --}}
@section('content')
<div class="row">
    <div class="col-lg-12">
    @if ($errors->any())
        <div class="alert alert-warning alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h5><i class="icon fas fa-exclamation-triangle"></i>{{session('flash_warning')}}</h5>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    @if (Session::has('flash_success'))
        <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h5><i class="icon fas fa-check"></i>{{session('flash_success')}}</h5>
        </div>
    @endif
    </div>
</div>
<div class="row">
                
    {{-- {!!'<pre>'!!} --}}
    {{-- {{print_r($users)}} --}}
    {{-- {!!'</pre>'!!} --}}
    <div class="col-lg-12">
        <div class="card card-primary card-outline">
            <div class="card-header">
              <h3 class="card-title">{{__('title.list_operator')}}</h3>

              <div class="card-tools">
                  {{$users->links('partials.pagination.adminlte')}}
              </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body p-0 h-scroll">
              <table class="table">
                <thead>
                    <tr>
                        <th style="width: 10px">#</th>
                        <th>{{__('table.name')}}</th>
                        <th>{{__('table.email')}}</th>
                        <th>{{__('table.telp')}}</th>
                        <th>{{__('table.status')}}</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                @foreach ($users as $user)
                    <tr>
                        <td>{{$loop->iteration}}</td>
                        <td>{{$user->name}}</td>
                        <td>{{$user->email}}</td>
                        <td>{{$user->telp}}</td>
                        <td><input type="checkbox" name="my-bs-status" data-url="{{route('toggle-delete',$user->id)}}" {{!$user->trashed() ? 'checked':''}} data-bootstrap-switch data-off-color="danger" data-on-color="success"></td>
                        <td>
                            <div class="btn-group" style="width:90px !important">
                                <button type="button" class="btn btn-outline-success" data-toggle="modal" data-target="#operator-edit-{{$user->id}}">{{__('button.edit')}}</button>
                                <button type="button" class="btn btn-outline-success" data-toggle="modal" data-target="#operator-detail-{{$user->id}}">{{__('button.detail')}}</button>
                            </div>
                        </td>
                    </tr>
                    @include('partials.modal.operator-edit',['id'=>$user->id,'name'=>$user->name,'email'=>$user->email,'telp'=>$user->telp,'address'=>$user->address])
                    @include('partials.modal.operator-detail',['id'=>$user->id,'name'=>$user->name,'email'=>$user->email,'telp'=>$user->telp,'address'=>$user->address])
                @endforeach
                </tbody>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
    </div>
<!-- /.col-md-6 -->
</div>
<!-- /.user -->
@endsection
@push('scripts')
<script>
    
    $(function () {
        $("[name='my-bs-status']").bootstrapSwitch({
            onSwitchChange: function(e, state) {
                // alert('change')
                let url = $(this).data('url');
                $.get(url, function (response) {
                    // console.log(response)
                    if (response.code == 200) {
                        $.each(response, function (i, obj) {
                        console.log(obj)
                            // $('#gaji_pokok').val(obj.gaji_pokok)
                        })
                    }
                },'json');
            }
        });
      $("input[data-bootstrap-switch]").each(function(){
        $(this).bootstrapSwitch('state', $(this).prop('checked'));
      })
    })
</script>
@endpush