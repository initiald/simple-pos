<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="{{route('dashboard')}}" class="brand-link">
      <img src="{{ asset("/bower_components/admin-lte/dist/img/AdminLTELogo.png") }}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
      <span class="brand-text font-weight-light">{!!config('adminlte.logo')!!}</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      {{-- <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="{{ asset("/bower_components/admin-lte/dist/img/user2-160x160.jpg") }}" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block">Alexander Pierce</a>
        </div>
      </div> --}}

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          @foreach (config('adminlte.sidebar_menu.administrator') as $item)
            @if (isset($item['header']))
                <li class="nav-header">{{__('menu.'.$item['header'])}}</li>
            @else
                @if (isset($item['submenu']))
                    {{-- tree menu --}}
                    <li class="nav-item menu-close">
                        <a href="{{$item['url']}}" class="nav-link">
                            <i class="nav-icon fas fa-tachometer-alt {{isset($item['icon_color']) ? 'text-'.$item['icon_color'] : ''}}"></i>
                            <p>
                                {{__('menu.'.$item['text'])}}
                                <i class="right fas fa-angle-left"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview sub-menu">
                            @foreach ($item['submenu'] as $level1)
                                @if (isset($level1['submenu']))
                                    <li class="nav-item">
                                        <a href="{{$item['url']}}" class="nav-link">
                                            <i class="nav-icon far fa-dot-circle text-danger"></i>
                                            <p>
                                                {{__('menu.'.$level1['text'])}}
                                                <i class="right fas fa-angle-left"></i>
                                            </p>
                                        </a>
                                        <ul class="nav nav-treeview sub-menu">
                                            @foreach ($level1['submenu'] as $level2)
                                                @if (isset($level2['submenu']))
                                                    
                                                    <li class="nav-item">
                                                        <a href="{{$item['url']}}" class="nav-link">
                                                            <i class="far fa-dot-circle text-warning nav-icon"></i>
                                                            <p>
                                                                {{__('menu.'.$level2['text'])}}
                                                            <i class="right fas fa-angle-left"></i>
                                                            </p>
                                                        </a>
                                                        <ul class="nav nav-treeview sub-menu">
                                                            @foreach ($level2['submenu'] as $level3)
                                                                <li class="nav-item">
                                                                    <a href="{{$item['url']}}" class="nav-link">
                                                                        <i class="far fa-dot-circle text-success nav-icon"></i>
                                                                        <p>{{__('menu.'.$level3['text'])}}</p>
                                                                    </a>
                                                                </li>
                                                            @endforeach
                                                        </ul>
                                                    </li>
                                                @else
                                                    <li class="nav-item">
                                                        <a href="{{$item['url']}}" class="nav-link">
                                                            <i class="far fa-dot-circle text-warning nav-icon"></i>
                                                            <p>{{__('menu.'.$level2['text'])}}</p>
                                                        </a>
                                                    </li>        
                                                @endif
                                            @endforeach
                                        </ul>
                                    </li>
                                @else
                                    <li class="nav-item">
                                        <a href="{{$level1['url']}}" class="nav-link">
                                            <i class="far fa-dot-circle text-danger nav-icon"></i>
                                            <p>{{__('menu.'.$level1['text'])}}</p>
                                        </a>
                                    </li>
                                @endif
                            @endforeach
                        </ul>
                    </li>
                @else
                    {{-- simple menu --}}
                    <li class="nav-item">
                        <a href="{{$item['url']}}" class="nav-link">
                        <i class="nav-icon {{isset($item['icon']) ? $item['icon']:'fas fa-th'}} {{isset($item['icon_color']) ? 'text-'.$item['icon_color'] : ''}}"></i>
                        <p>
                            {{__('menu.'.$item['text'])}}
                            {{-- <span class="right badge badge-danger">New</span> --}}
                        </p>
                        </a>
                    </li>        
                @endif 
            @endif
          @endforeach
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>