<div class="modal fade" id="member-edit-{{$id}}">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">{{__('title.edit_member')}}</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{route('member-update',$id)}}" method="post">
                @method('PUT')
                @csrf
                <input type="hidden" name="id" value="{{$id}}">
                <div class="modal-body">
                    <div class="input-group mb-3">
                        <input type="text" name="name" class="form-control" placeholder="Name" value="{{$name}}">
                        <div class="input-group-append">
                            <div class="input-group-text">
                            <span class="fas fa-user"></span>
                            </div>
                        </div>
                    </div>
                    <div class="input-group mb-3">
                        <input type="email" name="email" class="form-control" placeholder="Email" value="{{$email}}">
                        <div class="input-group-append">
                            <div class="input-group-text">
                            <span class="fas fa-envelope"></span>
                            </div>
                        </div>
                    </div>
                    <div class="input-group mb-3">
                        <input type="text" name="telp" class="form-control" placeholder="Telp" value="{{$telp}}">
                        <div class="input-group-append">
                            <div class="input-group-text">
                            <span class="fas fa-phone-square"></span>
                            </div>
                        </div>
                    </div>
                    <div class="input-group mb-3">
                        <input type="text" name="address" class="form-control" placeholder="Address" value="{{$address}}">
                        <div class="input-group-append">
                            <div class="input-group-text">
                            <span class="fas fa-address-card"></span>
                            </div>
                        </div>
                    </div>
                    <div class="input-group mb-3">
                        <input type="password" name="password" class="form-control" placeholder="Current Password">
                        <div class="input-group-append">
                            <div class="input-group-text">
                            <span class="fas fa-lock"></span>
                            </div>
                        </div>
                    </div>
                    <div class="input-group mb-3">
                        <input type="password" name="new_password" class="form-control" placeholder="New Password">
                        <div class="input-group-append">
                            <div class="input-group-text">
                            <span class="fas fa-lock"></span>
                            </div>
                        </div>
                    </div>
                    <div class="input-group mb-3">
                        <input type="password" name="retype_password" class="form-control" placeholder="Retype Password">
                        <div class="input-group-append">
                            <div class="input-group-text">
                            <span class="fas fa-lock"></span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default" data-dismiss="modal">{{__('button.close')}}</button>
                <button type="submit" class="btn btn-primary">{{__('button.save')}}</button>
                </div>
            </form>
        </div>
    </div>
</div>
@push('scripts')
<script>
    
    // function save() {
    //     console.log('test clicked')
    //     let id = $("input[name=id]").val();
    //     let name = $("input[name=name]").val();
    //     let email = $("input[name=email]").val();
    //     let telp = $("input[name=telp]").val();
    //     let address = $("input[name=address]").val();
    //     let csrf = $("input[name=_token]").val();
    //     let url = $("input[name=url]").val();
    //     let data = {id:id,name:name,email:email,telp:telp,address:address,_token:csrf,_method:'PUT'};
    //     console.log(data)
    //     $.post(url,data, function(data, status){
    //         // alert("Data: " + data + "\nStatus: " + status);

    //         console.log(data)
    //         console.log(status)
    //     });
    // }
</script>
@endpush