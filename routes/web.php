<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;

use App\Http\Controllers\DashboardController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\Auth\AuthController;
use App\Http\Controllers\UserController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/starter', function (Request $request) {
    // $request->session()->put();
    // dd($request->session()->all());
    return view('layouts.app');
})->name('dashboard');

// Route::get('/home', 'App\Http\Controllers\DashboardController@index')->name('home');
Route::get('/login', [AuthController::class, 'index'])->name('login');
Route::post('/authenticate', [AuthController::class, 'authenticate'])->name('auth');
Route::get('/member/register', [UserController::class, 'index'])->name('register');
// group
Route::middleware(['auth'])->group(function () {
    Route::get('/logout', [AuthController::class, 'logout'])->name('logout');
    Route::get('/dashboard', [DashboardController::class, 'index'])->name('dashboard');
    // products
    Route::get('/product', [ProductController::class, 'index'])->name('product');
    Route::get('/product/category', [ProductController::class, 'product_category'])->name('product_category');
    // management users
    Route::get('/register/user', [UserController::class, 'admin_register_user'])->name('register-user');
    Route::post('/register/user', [UserController::class, 'store_register_user'])->name('store-register-user');
    Route::get('/operator', [UserController::class, 'admin_operator'])->name('operator');
    Route::put('/operator/{id}/update', [UserController::class, 'admin_operator_update'])->name('operator-update');
    Route::put('/member/{id}/update', [UserController::class, 'admin_member_update'])->name('member-update');
    Route::get('/member', [UserController::class, 'admin_member'])->name('member');
    Route::get('/toggle/delete/{id}', [UserController::class, 'admin_user_toggle_delete'])->name('toggle-delete');
    // Route::get('/activate/{id}', [UserController::class, 'admin_user_activate'])->name('activate');

    Route::prefix('member')->group(function () {
        Route::get('/user', function () {// route('member/user')
            // Matches The "/admin/users" URL
        });
    });
});

Route::get('/flights', function () {
    // Only authenticated users may access this route...
})->middleware('auth');


