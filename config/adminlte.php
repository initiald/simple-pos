<?php

return [
    'title' => 'AdminLTE 3',
    'title_prefix' => '',
    'title_postfix' => '',
    // logo
    'logo' => '<b>Admin</b>LTE',
    'logo_img' => 'vendor/adminlte/dist/img/AdminLTELogo.png',
    'logo_img_class' => 'brand-image img-circle elevation-3',
    'logo_img_xl' => null,
    'logo_img_xl_class' => 'brand-image-xs',
    'logo_img_alt' => 'AdminLTE',

    // 'usermenu_enabled' => true,
    // 'usermenu_header' => false,
    // 'usermenu_header_class' => 'bg-primary',
    // 'usermenu_image' => false,
    // 'usermenu_desc' => false,
    // 'usermenu_profile_url' => false,
    // layout
    'layout_topnav' => null,
    'layout_boxed' => null,
    'layout_fixed_sidebar' => null,
    'layout_fixed_navbar' => null,
    'layout_fixed_footer' => null,
    'layout_dark_mode' => null,
    // authentication
    'classes_auth_card' => 'card-outline card-primary',
    'classes_auth_header' => '',
    'classes_auth_body' => '',
    'classes_auth_footer' => '',
    'classes_auth_icon' => '',
    'classes_auth_btn' => 'btn-flat btn-primary',
    'remember_me' => false,
    'tac' => false,
    'signup_fb' => false,
    'signup_google' => false,
    'signin_fb' => false,
    'signin_google' => false,
    'forgot_password' => false,
    // admin panel
    'classes_body' => '',
    'classes_brand' => '',
    'classes_brand_text' => '',
    'classes_content_wrapper' => '',
    'classes_content_header' => '',
    'classes_content' => '',
    'classes_sidebar' => 'sidebar-dark-primary elevation-4',
    'classes_sidebar_nav' => '',
    'classes_topnav' => 'navbar-white navbar-light',
    'classes_topnav_nav' => 'navbar-expand',
    'classes_topnav_container' => 'container',
    // left sidebar
    'sidebar_mini' => 'lg',
    'sidebar_collapse' => false,
    'sidebar_collapse_auto_size' => false,
    'sidebar_collapse_remember' => false,
    'sidebar_collapse_remember_no_transition' => true,
    'sidebar_scrollbar_theme' => 'os-theme-light',
    'sidebar_scrollbar_auto_hide' => 'l',
    'sidebar_nav_accordion' => true,
    'sidebar_nav_animation_speed' => 300,
    // right sidebar
    'right_sidebar' => false,
    'right_sidebar_icon' => 'fas fa-cogs',
    'right_sidebar_theme' => 'dark',
    'right_sidebar_slide' => true,
    'right_sidebar_push' => true,
    'right_sidebar_scrollbar_theme' => 'os-theme-light',
    'right_sidebar_scrollbar_auto_hide' => 'l',
    // url
    'use_route_url' => false,
    'dashboard_url' => 'home',
    'logout_url' => 'logout',
    'login_url' => 'login',
    'register_url' => 'register',
    'password_reset_url' => 'password/reset',
    'password_email_url' => 'password/email',
    'profile_url' => false,
    // menu items
    'sidebar_menu' => [
        'administrator' => [
            // Sidebar items:
            // [
            //     'text'        => 'example',
            //     'url'         => 'admin/pages',
            //     'icon'        => 'far fa-fw fa-file',
            //     'icon_color'  => 'success',
            //     'label'       => 4,
            //     'label_color' => 'success',
            // ],
            [
                'text' => 'sales',
                'url'  => '/sales',
                'icon' => 'fa fa-shopping-cart',
            ],
            [
                'text' => 'suppliers',
                'url'  => '/suppliers',
                'icon' => 'fa fa-users',
            ],
            [
                'text' => 'header_products',
                'icon'    => 'fas fa-fw fa-share',
                'url'     => '#',
                'submenu' => [
                    
                    [
                        'text' => 'products',
                        'url'  => '/product',
                        'icon' => 'fas fa-fw fa-user',
                    ],
                    [
                        'text' => 'product_categories',
                        'url'  => '/product/category',
                        'icon' => 'fas fa-fw fa-lock',
                    ],
                ],
            ],
            [
                'text'    => 'management_users',
                'icon'    => 'fas fa-fw fa-share',
                'url'     => '#',
                'submenu' => [
                    [
                        'text' => 'members',
                        'url'  => '/member',
                    ],
                    [
                        'text' => 'operators',
                        'url'  => '/operator',
                    ],
                    [
                        'text' => 'add_new',
                        'url'  => '/register/user',
                    ],
                ],
            ],
            ['header' => 'labels'],
            [
                'text'       => 'important',
                'icon_color' => 'red',
                'url'        => '#',
            ],
            [
                'text'       => 'warning',
                'icon_color' => 'yellow',
                'url'        => '#',
            ],
            [
                'text'       => 'information',
                'icon_color' => 'cyan',
                'url'        => '#',
            ],
        ],
        'member' => [
            ['header' => 'labels'],
            [
                'text'       => 'important',
                'icon_color' => 'red',
                'url'        => '#',
            ],
            [
                'text'       => 'warning',
                'icon_color' => 'yellow',
                'url'        => '#',
            ],
            [
                'text'       => 'information',
                'icon_color' => 'cyan',
                'url'        => '#',
            ],
        ],
        'add_your_roles' => []
    ],

    // Plugins Initialization
    'plugins' => [
        'Datatables' => [
            'active' => false,
            'files' => [
                [
                    'type' => 'js',
                    'asset' => false,
                    'location' => '//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js',
                ],
                [
                    'type' => 'js',
                    'asset' => false,
                    'location' => '//cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js',
                ],
                [
                    'type' => 'css',
                    'asset' => false,
                    'location' => '//cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css',
                ],
            ],
        ],
        'Select2' => [
            'active' => false,
            'files' => [
                [
                    'type' => 'js',
                    'asset' => false,
                    'location' => '//cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js',
                ],
                [
                    'type' => 'css',
                    'asset' => false,
                    'location' => '//cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.css',
                ],
            ],
        ],
        'Chartjs' => [
            'active' => false,
            'files' => [
                [
                    'type' => 'js',
                    'asset' => false,
                    'location' => '//cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.0/Chart.bundle.min.js',
                ],
            ],
        ],
        'Sweetalert2' => [
            'active' => false,
            'files' => [
                [
                    'type' => 'js',
                    'asset' => false,
                    'location' => '//cdn.jsdelivr.net/npm/sweetalert2@8',
                ],
            ],
        ],
        'Pace' => [
            'active' => false,
            'files' => [
                [
                    'type' => 'css',
                    'asset' => false,
                    'location' => '//cdnjs.cloudflare.com/ajax/libs/pace/1.0.2/themes/blue/pace-theme-center-radar.min.css',
                ],
                [
                    'type' => 'js',
                    'asset' => false,
                    'location' => '//cdnjs.cloudflare.com/ajax/libs/pace/1.0.2/pace.min.js',
                ],
            ],
        ],
    ],
    // livewire
    'livewire' => true,
    // my config
    'left_navbar' => [
        // [
        //     'text' => 'Home',
        //     'url' => 'index.html'
        // ],
        // [
        //     'text' => 'Contact',
        //     'url' => 'contact.html'
        // ],
        ],

    'right_navbar' => [
        'search' => false,
        'notification_chat' => false,
        'notification' => false,
        'profile' => true,
        'fullscreen' => false,
        'controll_sidebar' => false,
    ]
];
